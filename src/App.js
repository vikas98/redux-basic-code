import { useEffect, useState } from 'react';
import { connect, useDispatch, useSelector } from 'react-redux';
import {buyBook} from './redux'
import {findUser} from './redux' 

function App(props) {
  const [no, setNo] = useState(1)
  // using useSelector to get value from the reducer/state
  const noOfBooks = useSelector(state => state.bookReducer.noOfBooks);
  const {isLoading, element, error} = useSelector(state => state.userReducer)
  // using dispatch to call the action function
  const dispatch = useDispatch();
    return (
    <>
    {/* bu using connect define below */}
    <div>
    <h1>With Connect</h1>
      <h2> Buy Books </h2><p>available books... {props.noOfBooks}</p>
      <button onClick={()=>props.buyBook(no)} >Buy</button>
    </div>
    <div>

     </div>
     <div>
     {/* by using useSelector hook  */}
       <h1>With Hooks</h1>
       <h2> Buy Books </h2><p>available books... {noOfBooks}</p>
       <input type='text' value={no} onChange={(e)=> setNo(e.target.value)} />
      <button onClick={()=>dispatch(buyBook(no))} >Buy</button>

     </div>
     <div>
     {/* users */}
       <h1>Users</h1>
       <button onClick={()=>dispatch(findUser())} >Get User</button>
       <h6>
       {/* to check the loading value */}
         {
           isLoading && 
           <p>loading....</p>
         }
         {/* to show erros from the state */}
         {
           error && 
           <p>{error}</p>
         }
         {/* to show the result from the state */}
         {
           element && 
           <p>{element}</p>
         }
       </h6>
     </div>
    </>
  );
}

// using this to get value from the store
const mapStateToProps = (state) =>{
  return {
    noOfBooks: state.bookReducer.noOfBooks
  }
}
// using this to call the action function without hooks
const mapDispatchToProps = (dispatch) =>{
  return {
    buyBook: function(no){
      dispatch(buyBook(no));
    }
  }
}
//using connect to connect both mapStateToProps and mapDispatchToProps
export default connect(mapStateToProps,mapDispatchToProps) (App);
