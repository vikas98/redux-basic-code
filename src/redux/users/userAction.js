import axios from "axios"
import { USER_ERROR, USER_LOADING, USER_SUCCESS } from "./userConstant"

//find user function to perform actions
 export const findUser = () =>async (dispatch) =>{
    dispatch({type:USER_LOADING})
    axios.get('http://localhost:2100/admin/userPost')
    .then(response=>{
        const user = response.data.data.map(user=>user.name)
        dispatch({type:USER_SUCCESS, payload: user})
    })
    .catch(error=>{
        dispatch({type:USER_ERROR, payload:error.message})
    })
}