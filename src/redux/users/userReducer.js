import { USER_ERROR, USER_LOADING, USER_SUCCESS } from "./userConstant"
//initial state in userReducer
const initialState = {
    isLoading: false,
    element:[],
    error:''
}

//userReducer switching between diffrent action types
const userReducer = (state=initialState, action)=>{
    switch(action.type){
        case USER_LOADING:
            return{
                ...state,
                isLoading: true
            }
        case USER_SUCCESS:
            return {
                isLoading:false,
                element: action.payload,
                error:''
            }    
        case USER_ERROR:
            return {
                isLoading: false,
                element:[],
                error:action.payload
            } 
        default:
            return(
                state
            )    
    }
}

export default userReducer