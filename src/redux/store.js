import { applyMiddleware, createStore } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";
import rootReducer from "./rootReducer";

//creating store througn create store from create store 
// applymiddleware to use thunk to perform async operations
//compose with devtoos to use chrome extension of redux
const store= createStore(rootReducer, composeWithDevTools(applyMiddleware(thunk)));

export default store