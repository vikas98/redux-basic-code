import { combineReducers } from "redux";
import bookReducer from "./books/bookReducer";
import userReducer from "./users/userReducer";

// combining reducer through combine reducer
 const rootReducer = combineReducers({
    bookReducer: bookReducer,
    userReducer: userReducer
});

export default rootReducer