import { BUY_BOOK } from "./bookConstant"

//initial state in book reducer
const initialState = {
    noOfBooks: 10
}
//bookReducer switching between diffrent action types
const bookReducer = (state=initialState, action)=>{
    switch(action.type) {
        case BUY_BOOK: 
        return{
            ...state,
            noOfBooks: state.noOfBooks-(action.payload)
        }
        default: 
        return(
            state)
        
    }
}


export default bookReducer