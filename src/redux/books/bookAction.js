import { BUY_BOOK } from "./bookConstant"

// buy book function to perform the action
export const buyBook = (number=1) =>{
    return {
        type: BUY_BOOK,
        payload: number
    }
}